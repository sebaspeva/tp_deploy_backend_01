// winston its a logger that allows to write the logs in a prettier way
const winston = require('winston');
// import config from '@/config';

const transports = [];
if(process.env.NODE_ENV !== 'development') {
  transports.push(
    new winston.transports.Console(({
        format: winston.format.simple(),
      }))
  )
} else {
  transports.push(
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.cli(),
        winston.format.splat(),
      )
    })
  )
}

const LoggerInstance = winston.createLogger({
//   level: config.logs.level,
  levels: winston.config.npm.levels,
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    winston.format.errors({ stack: true }),
    winston.format.splat(),//Permite que se puedan pasar params al texto del log
    winston.format.json(),
    winston.format.prettyPrint()
  ),
  transports
});

module.exports = LoggerInstance;