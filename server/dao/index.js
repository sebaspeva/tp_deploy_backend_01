const Logger = require('../logger/logger');
const config = require('../config/config');

const mysql = require('mysql2');
const fs = require('fs');

if (config.environment != 'TEST') {
  
  var dbConnection = mysql.createConnection({
    host: config.db.host,
    user: config.db.username,
    password: config.db.password,
    database: config.db.database,
    multipleStatements: true
  });

} else {
  
  var dbConnection = mysql.createConnection({
    host: config.db.host,
    user: config.db.username,
    password: config.db.password,
    database: config.db.database,
    multipleStatements: true,
    ssl:{

      ca:fs.readFileSync(config.db.azure_Ssl)

    },

  });

}

dbConnection.connect((err)=> {
  if(!err)
    Logger.info(`
      ##################################################
      🛡️ Database connection established successfully 🛡️
      ##################################################
    `);
  else
  
    Logger.info(`
      ##################################################
      🛡️ COULD NOT ESTABLISH CONNECTION WITH DB 🛡️
      ${JSON.stringify(err,undefined,2)}
      ##################################################
    `);
  }
);

module.exports = {
  dbConnection,
};