const db = require('./index').dbConnection;

module.exports = {

  addIngresos(req, res) {
    result = db.query(`INSERT INTO Transacción (Fecha, Transacción, Descripción, Monto, Frecuencia, Aumento)
    VALUES
    (NOW(),?, ?, ?, ?, ?)`, 
    [req.body.Transacción, req.body.Descripción ,req.body.Monto, req.body.Frecuencia, req.body.Aumento],
    (err, result) => {
      if (!err)
        res.status(201).send(result);
      else
        res.status(400).send(err)
      })
  },      

  addEgresos(req, res) {
    result = db.query(`INSERT INTO Transacción (Fecha, Transacción, Descripción, Monto, Frecuencia, Aumento)
    VALUES
    (NOW(),?, ?, ?, ?, ?)`, 
    [req.body.Transacción, req.body.Descripción ,req.body.Monto, req.body.Frecuencia, req.body.Aumento],
    (err, result) => {
      if (!err)
        res.status(201).send(result);
      else
        res.status(400).send(err)
      })
  },    
  

  Delete(req, res) {
    db.query('DELETE FROM Transacción WHERE id = ?',
    [req.params.id],
     (err, rows) => {
      if (!err)
        res.status(200).send(rows);
      else
        res.status(400).send(err)
      })
  },

  listIngresos(req, res) {
    db.query('SELECT id, Transacción, date_format(Fecha, "%d-%m-%y") AS Fecha, Descripción, Monto, Frecuencia, Aumento FROM diplo_bd_01.transacción WHERE Transacción= "Ingresos" ORDER BY id DESC LIMIT 10 ', (err, rows) => {
      if (!err)
        res.status(200).send(rows);
      else
        res.status(400).send(err)
      })
  },

  listEgresos(req, res) {
    db.query('SELECT id, Transacción, date_format(Fecha, "%d-%m-%y") AS Fecha, Descripción, Monto, Frecuencia, Aumento FROM diplo_bd_01.transacción WHERE Transacción= "Egresos" ORDER BY id DESC LIMIT 10', (err, rows) => {
      if (!err)
        res.status(200).send(rows);
      else
        res.status(400).send(err)
      })
  },


  lastIngresos(req, res) {
    db.query('SELECT Monto, Fecha, Descripción FROM diplo_bd_01.transacción WHERE Transacción= "Ingresos" ORDER BY id DESC LIMIT 1',
     (err, rows) => {
      if (!err)
        res.status(200).send(rows);
      else
        res.status(400).send(err)
      })
  },

  listTransacción(req, res) {
    db.query('SELECT id, Transacción, date_format(Fecha, "%d-%m-%y") AS Fecha, Descripción, Monto, Frecuencia, Aumento FROM diplo_bd_01.transacción order by id DESC, id LIMIT 10',
    (err, rows) => {
      if (!err)
        res.status(200).send(rows);
      else
        res.status(400).send(err)
      })
  },
  

  listTransacción1(req, res) {
    db.query('SELECT date_format(Fecha, "%d-%m-%y") AS Fecha, Monto FROM diplo_bd_01.transacción WHERE Transacción= "Ingresos" order by id DESC, id LIMIT 10',
    (err, rows) => {
      if (!err)
        res.status(200).send(rows);
      else
        res.status(400).send(err)
      })
  },

  listTransacción2(req, res) {
    db.query('SELECT date_format(Fecha, "%d-%m-%y") AS Fecha, Monto * (-1) AS Monto FROM diplo_bd_01.transacción WHERE Transacción= "Egresos" order by id DESC, id LIMIT 10',
    (err, rows) => {
      if (!err)
        res.status(200).send(rows);
      else
        res.status(400).send(err)
      })
  },

  
  // listTransacción(req, res) {
  //   db.query('SELECT * FROM quick_account.transacción order by Fecha DESC, id LIMIT 10',
  //   (err, rows) => {
  //     if (!err)
  //       res.status(200).send(rows);
  //     else
  //       res.status(400).send(err)
  //     })
  // },

  update(req, res) {
    result = db.query(`UPDATE autos 
    SET marca = ?, modelo = ?, color = ?, patente = ?, fechaFabricacion = ? 
    WHERE idAutos = ?`, 
    [req.body.marca, req.body.modelo, req.body.color, req.body.patente, req.body.fechaFabricacion, req.params.idAutos],
    (err, result) => {
      if (!err)
        res.status(200).send(result);
      else
        res.status(400).send(err)
      })
  },

  destroy(req, res) {
    db.query('DELETE FROM autos WHERE idAutos = ?',
    [req.params.idAutos],
     (err, result) => {
      if (!err)
        res.status(200).send(result);
      else
        res.status(400).send(err)
      })
  },

};

