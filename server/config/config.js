// dotenv is what allow us to handle some  configuration properties
const dotenv = require('dotenv');

// Just in case init
process.env.PORT = '8000';
process.env.NODE_ENV = 'DEV';

// Loads .env config values
const envFound = dotenv.config();
if (envFound.error) {
    // This error should crash whole process
    throw new Error("⚠️  Couldn't find .env file  ⚠️");
  }

// Sets active environment
const environment = process.env.ACTIVE_ENVIRONMENT;

// Inits gral vars for PostgreSQL
let mysql_database = "";
let mysql_host = "";
let mysql_username = "";
let mysql_password = "";


if (environment == 'DEV') {
  mysql_database = process.env.DEV_DATABASE;
  mysql_host = process.env.DEV_HOST;
  mysql_username = process.env.DEV_USERNAME;
  mysql_password = process.env.DEV_PASSWORD;
} else if (environment == 'TEST') {
  mysql_database = process.env.TEST_DATABASE;
  mysql_host = process.env.TEST_HOST;
  mysql_username = process.env.TEST_USERNAME;
  mysql_password = process.env.TEST_PASSWORD;
} else if (environment == 'PRD'){
  mysql_database = process.env.PRD_DATABASE;
  mysql_host = process.env.PRD_HOST;
  mysql_username = process.env.PRD_USERNAME;
  mysql_password = process.env.PRD_PASSWORD;
}


module.exports = {
    environment: environment,
    port: parseInt(process.env.PORT, 10),
    db: {
      database: mysql_database,
      host: mysql_host,
      username: mysql_username,
      password: mysql_password,

      azure_Ssl: __dirname + '/DigiCertGlobalRootCA.crt.pem',
    },

}