const quickAccountDao = require('../dao/quickAccountDao');
const autosDao = require('../dao/quickAccountDao');

module.exports = (app) => {

  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the API!',
  }));

  /*
    This section does the following:
      *First of all, the 'app.all' part is telling Express Router to catch every HTTP VERB that is requested to the server (GET,POST,PUT,DELETE,etc).
      *The first parameter makes that it cathes all the requests, independently of whatever the request URI was (this way, 
      it's ignoring if the request was made to '/' or '/api' or '/whatever', it will catch them all).
      *The second part is an unnamed function that receives 3 parameters. The first one ('req') has all the data related to the request (request header, body,etc).
      The second parameter is used to define the data that we will send as a response (response header, body, etc).
      The third parameter enables the ability to handle the same request by a 2nd callback. Without this parameter, we cannot make the 'next()' call.
      The next parameter is needed in here for one main and important reason, if we wouldn't use it, all the requests would be catched only by this Router instruction
      and none other
  */
  app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
  });

  
  /*
    Express routing is a bit curious:
    Take for example, the following Router instruction:
          app.get('/api/todos/:todoId', todosController.retrieve);
    Route parameters are named URL segments (i.e.: ':todoId') that are used to capture the values specified at their position in the URL. 
    The captured values are populated in the req.params object, 
    with the name of the route parameter specified in the path as their respective keys. (i.e.: 'req.params.todoId').
    Other examples (from official docs): 
          Route path: /users/:userId/books/:bookId
          Request URL: http://localhost:3000/users/34/books/8989
          req.params: { "userId": "34", "bookId": "8989" }
          ---------------------------------------------------------
          Route path: /flights/:from-:to    ---> NOTE THAT: Express Router interprets hyphens ('-') and dots ('.') as literals
          Request URL: http://localhost:3000/flights/LAX-SFO
          req.params: { "from": "LAX", "to": "SFO" }
    So, if we would like to use that original Router instruction URI, 
    we should write it after all the others Router instruction URI's that begins with '/api/todos/' and that uses the HTTP VERB 'GET'. Take into account
    that router instructions are positionals, it analyzes the first instruction and then the second instruction and so on until it reaches a 
    Router instruction URI that matches with the one from the request.
  */
  // app.post('/autos/create', autosDao.create);
  app.get('/ingresos/list', quickAccountDao.listIngresos);
  app.get('/egresos/list', quickAccountDao.listEgresos);
  app.get('/ingresos/last', quickAccountDao.lastIngresos);
  app.get('/transacciones/list', quickAccountDao.listTransacción);
  app.get('/transacciones/list1', quickAccountDao.listTransacción1);
  app.get('/transacciones/list2', quickAccountDao.listTransacción2);
  app.post('/ingresos/add', quickAccountDao.addIngresos);
  app.post('/egresos/add', quickAccountDao.addEgresos);
  app.delete('/ingresos/del', quickAccountDao.Delete);
  app.delete('/egresos/del', quickAccountDao.Delete);




  // app.get('/autos/:idAutos', autosDao.retrieve);
  // app.put('/autos/:idAutos', autosDao.update);
  // app.delete('/autos/:idAutos', autosDao.destroy);
};