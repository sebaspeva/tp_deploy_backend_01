# Consideraciones para hacer un deploy

1. Tener el código funcionando a nivel local
2. Subir la base de datos a Azure
3. Actualizar datos en el backend
<br>

    > ACTIVE_ENVIROMENT (descomentar el que se use)
<br>
  NOMBRE, HOST, USERNAME Y PASS (de Azure)
<ul>

---

<details><summary>Ver referencia del código</summary>
<p>

```JavaScript
TEST_DATABASE='diplo_bd_01'
TEST_HOST='server-azure.mysql.database.azure.com'
TEST_USERNAME='Sebaazure'
TEST_PASSWORD='***********'

#Active environment
#ACTIVE_ENVIRONMENT='DEV'
ACTIVE_ENVIRONMENT='TEST'
#ACTIVE_ENVIRONMENT='PRD'
```

</p>
</details>

---
</ul>
<div id="gitlab">

4. Subir backend a repositorio Gitlab : 
<br>  

```console 
Git init -b main
Git add .
Git commit -m “comentario”
Git remote add origin https://gitlab.com/sebaspeva/...
Git push -uf origin main
```
<div>

<div id="vercel"> 

5. Loggearse en  [Vercel](https://vercel.com/dashboard) a través de Gitlab

>+ Add new...

>+ Project

>+ Continue with Gitlab

>+ Import

>+ Deploy

</div>

6. Chequear conexión con Postman o, desde el front, trabajando local. Asegurarse de cambiar las URL del Backend por la de Vercel

<ul>

---

<details><summary>Ver referencia del código</summary>
<p>

```JavaScript
const routes ={
    //URL: "http://127.0.0.1:8000",
   URL: "https://tp-deploy-backend-01.vercel.app",
    
    ADD_INCOME: "/ingresos/add",
    REMOVE_INCOME: "/ingresos/remove",
    LAST_INCOME: "/ingresos/last",
    LIST_INCOME: "/ingresos/list",
    ADD_EXPENSE: "/egresos/add",
    REMOVE_EXPENSE: "/egresos/remove",
    LAST_EXPENSE:"/egresos/last",
    LIST_EXPENSE:"/egresos/list",
    LIST_TRANSACCIONES:"/transacciones/list",
    DELETE_INGRESO:"/ingresos/del",
    DELETE_EGRESO:"/egresos/del",
    CHART_INCOME:"/transacciones/list1",
    CHART_EXPENSE:"/transacciones/list2"
}

export default routes
```

</p>
</details>

---

</ul>


7. Repetir el proceso <a href="#gitlab"> #4</a>  y <a href="#vercel"> #5</a> pero con el Front

8. Visualizar página final desde el link del front de [Vercel](https://vercel.com/dashboard) 
