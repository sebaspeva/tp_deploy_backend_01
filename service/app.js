// express is what allow us to receive the requests and handle the responses
const express = require('express');
// morgan its an http requests logger (it's focus is logging only http related stuff)
const logger = require('morgan');
// body-parser is a middleware that checks and validates the incoming body requests (so that they match what we expect)
const bodyParser = require('body-parser');

// Set up the express app
const app = express();

// Log requests to the console.
app.use(logger('dev'));

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json()); // Accepts json body
app.use(bodyParser.urlencoded({ extended: false })); // Accepts urlencoded body

// Require our routes into the application.
require('../server/routes')(app);
app.get('/status', (req, res) => res.status(200).send({
  message: '⬆️ Server is up ⬆️ ',
}));

module.exports = app;