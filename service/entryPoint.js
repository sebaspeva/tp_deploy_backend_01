// This will be our application entry. We'll setup our service entry point here.
const http = require('http');
const app = require('./app'); // The express app we just created
const Logger = require('../server/logger/logger'); //Some pretty logger module
const config = require('../server/config/config'); //Configuration loader module

app.set('port', config.port);

// The line below creates the API using the app module (in there it's definded everything, here we only define the API)
const server = http.createServer(app);
// Establish the port in which the app should be listening for the requests 
server.listen(config.port);
Logger.info(`
      ################################################
      🛡️  Server listening on port: ${config.port} 🛡️
      🛡️  Server environment: ${config.environment} 🛡️
      ################################################
    `);
// Logger.info(`Active env: ${process.env.NODE_ENV}`);